import numbers
from array import array
import reprlib
import math


class Vector:
    typecode = 'd'
    shortcut_names = 'xyzt'

    def __init__(self, componenets):
        self._componenets = array(self.typecode, componenets)

    def __iter__(self):
        return iter(self._componenets)

    def __repr__(self):
        components = reprlib.repr(self._componenets)
        components = components[components.find('['):-1]
        return 'Vector({})'.format(components)

    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +
                bytes(self._componenets))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __abs__(self):
        return math.sqrt(sum(x * x for x in self))

    def __bool__(self):
        return bool(abs(self))

    def __len__(self, other):
        return len(self._componenets)

    def __getitem__(self, item):
        cls = type(self)
        if isinstance(item, slice):
            return cls(self._componenets[item])
        elif isinstance(item, numbers.Integral):
            return self._componenets[item]
        else:
            msg = '{cls.__name__} indices must be intergers'
            raise TypeError(msg.format(cls=cls))

    def __getattr__(self, item):
        cls = type(self)
        if len(item) == 1:
            pos = cls.shortcut_names.find(item)
            if 0 <= pos < len(self._componenets):
                return self._componenets[pos]
        msg = '{.__name__!r} object has no attribute {!r}'
        raise AttributeError(msg.format(cls, item))

    def __setattr__(self, key, value):
        cls = type(self)
        if len(key) == 1:
            if key in cls.shortcut_names:
                error = 'readonly attribute {attr_name!r}'
            elif key.islower():
                error = "can't set attributes 'a' to 'z' in {cls_name!r}"
            else:
                error = ''
            if error:
                msg = error.format(cls_name=cls.__name__, attr_name=key)
                raise AttributeError(msg)
        super().__setattr__(key, value)

    @classmethod
    def frombytes(cls, octets):
        typecode = chr(octets[0])
        memv = memoryview(octets[1:]).cast(typecode)
        return cls(memv)
