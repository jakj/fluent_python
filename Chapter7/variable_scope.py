b = 6

def f1(a):
	print(a)
	print(b)


def f2(a):
	global b
	print(a)
	print(b)
	b = 9
	print(b) # second time it takes on the new assignment


if __name__ == '__main__':
	f2(3)
